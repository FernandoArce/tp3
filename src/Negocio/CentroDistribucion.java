package Negocio;


public class CentroDistribucion extends Ubicacion
{
	double distanciaPromedio=0;
	public CentroDistribucion(double latitud, double longitud) {
		super(latitud, longitud);
		
	}
	public double getDistanciaPromedio() {
		return distanciaPromedio;
	}
	public void setDistanciaPromedio(double distanciaPromedio) {
		this.distanciaPromedio = distanciaPromedio;
	}
	

}