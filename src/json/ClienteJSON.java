package json;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import Negocio.Cliente;



public class ClienteJSON {
	ArrayList<Cliente> clientes = new ArrayList<Cliente>();
	

	public void agregarCliente(Cliente c) {
		clientes.add(c);
	}
	public ArrayList<Cliente> dameClientes() {
		return clientes;
	}
	
	public void generarJSON(String archivo) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();		
		try {
			FileWriter writer = new FileWriter(archivo);
			writer.write(gson.toJson(this));
			writer.close();
		} catch (Exception e) {

		}
	}
	
	public ClienteJSON leerJSON(String archivo)
	{
		Gson gson =new Gson();
		ClienteJSON ret =null;
		try
		{			
			ret = gson.fromJson(new BufferedReader(new FileReader(archivo)), ClienteJSON.class);
			}
		catch(Exception e){
			
		}
		return ret;
		}
}